import { Module } from '@nestjs/common';
import { CoordinateModule } from './coordinate/coordinate.module';

@Module({
  imports: [CoordinateModule],
  controllers: [],
  providers: [],
})
export class AppModule { }
