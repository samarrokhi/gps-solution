import { Inject, Injectable } from '@nestjs/common';
import * as moment from 'moment';
import { CoordinateRequestModel } from '../../models/coordinateReq.model';
import { MongodbRepositoryInterface } from '../../repositories/mongodb-repository';

export interface CoordinateServiceInterface {
    create(coordinateDto: CoordinateRequestModel): Promise<void>;
}

export class CoordinateService implements CoordinateService {
    constructor(@Inject('MongodbRepositoryInterface') private repository: MongodbRepositoryInterface) { }

    async create(coordinateDto: CoordinateRequestModel): Promise<void> {
        for (const coordinate of coordinateDto.coordinates) {
            await this.repository.create({ coordinate: coordinate, timestamp: moment.now() })
        }
    }
}