export interface BulkSaveInterface {
    succeed: boolean;
    messages: string[];
}