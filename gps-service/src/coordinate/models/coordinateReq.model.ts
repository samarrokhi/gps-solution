import { IsArray, IsLatLong } from "class-validator";

export class CoordinateRequestModel {
    @IsArray()
    @IsLatLong({
        each: true
    })
    coordinates: string[];
}