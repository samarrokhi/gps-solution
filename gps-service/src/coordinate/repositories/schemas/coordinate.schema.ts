import { Schema, model } from 'mongoose';

export interface CoordinateInterface {
  timestamp: Number,
  coordinate: String
}

export const coordinateSchema = new Schema<CoordinateInterface>({
  timestamp: { type: Number, required: true },
  coordinate: { type: String, required: true },
});
