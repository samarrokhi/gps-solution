import { Test, TestingModule } from '@nestjs/testing';
import { MongodbRepository } from './mongodb-repository';

describe('MongodbRepository', () => {
  let provider: MongodbRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MongodbRepository],
    }).compile();

    provider = module.get<MongodbRepository>(MongodbRepository);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
