import { Injectable } from '@nestjs/common';
import { model } from 'mongoose';
import * as mongoose from 'mongoose';
import { CoordinateInterface, coordinateSchema } from './schemas/coordinate.schema';

@Injectable()
export class MongodbRepository {
  constructor() {
    mongoose.connect('mongodb://localhost:27017/coordinates', { useNewUrlParser: true, useUnifiedTopology: true });
  }

  async create(createCoordinateDto: CoordinateInterface): Promise<void> {
    const CoordinateModel = model<CoordinateInterface>('coordinate', coordinateSchema);
    const doc = new CoordinateModel({ timestamp: createCoordinateDto.timestamp, coordinate: createCoordinateDto.coordinate })
    await doc.save();
  }
}

export interface MongodbRepositoryInterface {
  create(createCoordinateDto: CoordinateInterface): Promise<void>
}
