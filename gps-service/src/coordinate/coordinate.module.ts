import { Module } from '@nestjs/common';
import { CoordinateController } from './controllers/coordinate/coordinate.controller';
import { CoordinateService } from './services/coordinate/coordinate.service';
import { MongodbRepository } from './repositories/mongodb-repository';

@Module({
  controllers: [CoordinateController],
  providers: [{ provide: 'CoordinateServiceInterface', useClass: CoordinateService },
  { provide: 'MongodbRepositoryInterface', useClass: MongodbRepository }]
})
export class CoordinateModule { }
