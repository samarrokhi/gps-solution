import { Body, Controller, Inject } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { validate } from 'class-validator';
import { BulkSaveInterface } from 'src/coordinate/models/bulkSave.model';
import { CoordinateRequestModel } from 'src/coordinate/models/coordinateReq.model';
import { CoordinateServiceInterface } from 'src/coordinate/services/coordinate/coordinate.service';

@Controller('coordinate')
export class CoordinateController {

    constructor(@Inject('CoordinateServiceInterface') private coordinateService: CoordinateServiceInterface) { }

    @GrpcMethod('GpsService', 'BulkSave')
    async bulkSave(@Body() coordinates: CoordinateRequestModel): Promise<BulkSaveInterface> {
        const validationModel = new CoordinateRequestModel();
        validationModel.coordinates = coordinates.coordinates;
        const errors = await validate(validationModel, { validationError: { target: false, value: false } });
        
        if (errors.length > 0)
            return { succeed: false, messages: errors.map(a => a.constraints).map(a => a['isLatLong']) }

        const result = await this.coordinateService.create(coordinates);
        return { succeed: result != null, messages: [] };
    }
}
