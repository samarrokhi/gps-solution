import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport, MicroserviceOptions } from '@nestjs/microservices';
import { join } from 'path';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.GRPC,
      options: {
        package: 'gpsService',
        protoPath: join(__dirname, 'gps.proto'),
        url: 'localhost:5001'
      }
    }
  );
  app.listen(() => console.log('Microservice is listening'));
}
bootstrap();
