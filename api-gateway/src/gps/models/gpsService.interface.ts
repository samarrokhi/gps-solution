import { Observable } from "rxjs";

export interface RemoteGpsServiceInterface {
    bulkSave(data: { coordinates: string[] }): Observable<any>;
}

export interface BulkSaveResponseInterface {
    succeed: boolean;
    messages: boolean;
}