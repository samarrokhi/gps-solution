import { Body, Controller, Inject, Post } from '@nestjs/common';
import { GpsServiceInterface } from 'src/gps/services/gps/gps.service';

@Controller('gps')
export class GpsController {
    constructor(@Inject('GpsServiceInterface') private gpsService: GpsServiceInterface) { }

    @Post()
    bulkSave(@Body() coordinates: string[]) {
        return this.gpsService.bulkSave(coordinates);
    }
}
