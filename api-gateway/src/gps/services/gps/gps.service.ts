import { Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { Observable } from 'rxjs';
import { BulkSaveResponseInterface, RemoteGpsServiceInterface } from 'src/gps/models/gpsService.interface';

@Injectable()
export class GpsService implements OnModuleInit, GpsServiceInterface {

    private gpsService: RemoteGpsServiceInterface;

    constructor(@Inject('GPS_SERVICE') private client: ClientGrpc) { }

    onModuleInit() {
        this.gpsService = this.client.getService<RemoteGpsServiceInterface>('GpsService');
    }

    bulkSave(coordinates: string[]): Observable<any> {
        const result = this.gpsService.bulkSave({ coordinates: coordinates });
        return result;
    }
}

export interface GpsServiceInterface {
    bulkSave(coordinates: string[]): Observable<any>;
}