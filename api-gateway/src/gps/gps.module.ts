import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { GpsController } from './controllers/gps/gps.controller';
import { GpsService } from './services/gps/gps.service';

@Module({
  imports: [ClientsModule.register([
    {
      name: 'GPS_SERVICE',
      transport: Transport.GRPC,
      options: {
        package: 'gpsService',
        protoPath: join(__dirname, 'gps.proto'),
        url: 'localhost:5001'
      },
    },
  ]),
  ],
  controllers: [GpsController],
  providers: [{ provide: 'GpsServiceInterface', useClass: GpsService }]
})
export class GpsModule { }
